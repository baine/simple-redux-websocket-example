
import { createStore, applyMiddleware } from 'redux'
import createWebSocketMiddleware from 'simple-redux-websocket-middleware'

let reducer = (state = {}, action) => state

let actionLogger = store => next => action => {
  console.log(action)
  return next(action)
}

let coinbaseMiddleware = store => next => action => {
  if (action.type === 'WEBSOCKET/EVENT/OPEN' && action.payload.id === 'coinbase') {
    store.dispatch({
      type: 'WEBSOCKET/REQUEST/SEND',
      payload: {
        id: 'coinbase',
        message: JSON.stringify({
          type: 'subscribe',
          product_ids: [ 'BTC-USD' ],
          channels: [ 'level2', 'heartbeat' ]
        })
      }
    })
  }
  return next(action)
}

const store = createStore(
  reducer,
  {},
  applyMiddleware(
    createWebSocketMiddleware(),
    coinbaseMiddleware,
    actionLogger
  )
)

// connect to gemini immediately
store.dispatch({
  type: 'WEBSOCKET/REQUEST/OPEN',
  payload: {
    id: 'gemini',
    url: 'wss://api.gemini.com/v1/marketdata/btcusd'
  }
})

// at t=2 seconds connect to coinbase
setTimeout(() => store.dispatch({
  type: 'WEBSOCKET/REQUEST/OPEN',
  payload: {
    id: 'coinbase',
    url: 'wss://ws-feed.pro.coinbase.com'
  }
}), 2000)

// close gemini at t=5 seconds, close coinbase at t=10 seconds
let timeouts = [{ id: 'gemini', timeout: 5 }, { id: 'coinbase', timeout: 10 }]

timeouts.forEach(({ id, timeout }) => setTimeout(() => store.dispatch({
  type: 'WEBSOCKET/REQUEST/CLOSE',
  payload: {
    id
  }
}), timeout * 1000))
